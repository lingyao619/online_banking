/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-2016 abel533@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package tk.mybatis.springboot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.springboot.mapper.MenuMapper;
import tk.mybatis.springboot.model.Account;
import tk.mybatis.springboot.model.Menu;
import tk.mybatis.springboot.model.MenuVo;

import java.util.ArrayList;
import java.util.List;

@Service
public class MenuService {

    @Autowired
    private MenuMapper menuMapper;

    public List<MenuVo> getAll(Menu selmenu,Account account) {
        if("管理员".equalsIgnoreCase(account.getRole())){
            selmenu.setMenutype("1");
        }else{
            selmenu.setMenutype("0");
        }
        /*Example example=new Example(Menu.class);
        Example.Criteria criteria = example.createCriteria();
        if (selmenu.getMenutype()==1||selmenu.getMenutype()==0) {
            criteria.andLike("menutype", "%" + selmenu.getMenutype() + "%");
        }*/
        List<Menu> list=menuMapper.select(selmenu);
        List<MenuVo> mainList=new ArrayList<>();
        List<MenuVo> subList=new ArrayList<>();
        for (Menu menu:list) {
            if ("".equalsIgnoreCase(menu.getParentId()) || menu.getParentId()==null ){
                mainList.add(new MenuVo(menu));
            }else{
                subList.add(new MenuVo(menu));
            }
        }
        parseParentAndChilren(mainList,subList);
        return mainList;
    }

    private void parseParentAndChilren(List<MenuVo> mainList, List<MenuVo> subList){
        for (MenuVo main:mainList ) {
             List<MenuVo> list=new ArrayList<>();
             for (MenuVo sub:subList) {
                if (main.getId().equalsIgnoreCase(sub.getParentId())){
                    list.add(sub);
                }
            }
            main.setChildren(list);
        }
    }

  /*  public menu getById(Integer id) {
        return menuMapper.selectByPrimaryKey(id);
    }

    public void deleteById(Integer id) {
        menuMapper.deleteByPrimaryKey(id);
    }

    public void save(menu country) {
        if (country.getId() != null) {
            menuMapper.updateByPrimaryKey(country);
        } else {
            menuMapper.insert(country);
        }
    }*/
}
