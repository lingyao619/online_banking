package tk.mybatis.springboot.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import tk.mybatis.springboot.model.Account;
import tk.mybatis.springboot.model.Person;
import tk.mybatis.springboot.model.PersonVO;
import tk.mybatis.springboot.service.LoginService;
import tk.mybatis.springboot.service.PersonService;
import tk.mybatis.springboot.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author liuzh
 * @since 2015-12-19 11:10
 */
@Controller
@RequestMapping("/person")
public class PersonController {
    @Autowired
    private PersonService personService;

    //跳转所有账户页面
    @RequestMapping("/showAllPerson")
    public String showAllPerson(){
        return "/user/person";
    }

    //跳转冻结账户页面
    @RequestMapping("/showStopPerson")
    public String showStopPerson(){
        return "/user/personStop";
    }

    //跳转开户页面
    @RequestMapping("/newPerson")
    public String newPerson(){
        return "/user/newPerson";
    }

    //跳转启用账户页面
    @RequestMapping("/showOpenPerson")
    public String showOpenPerson(){
        return "/user/personOpen";
    }

    //获取所有人员信息
    @RequestMapping(value="/getAllPerson")
    @ResponseBody
    public  Map<String,Object> getAllPerson(PersonVO personVO) {
        List<PersonVO> personVOList= personService.getAllPerson(personVO);
        int total=personService.countAllPerson(personVO);
        Map<String,Object> map=new HashMap<>();
        map.put("rows",personVOList);
        map.put("total",total);
        return  map;
    }

   //修改状态信息
    @RequestMapping(value="/changeStatus")
    @ResponseBody
    public JsonResult<Integer>  changeStatus(HttpServletRequest request) {
        String id=(String)request.getParameter("id");
        String status=(String)request.getParameter("status");
        int result=personService.changeStatus(id,status);
        return new JsonResult<Integer>(result);
    }

    //删除个人信息
    @RequestMapping(value="/deleteInfo")
    @ResponseBody
    public JsonResult<Integer>  deleteInfo(HttpServletRequest request) {
        String id=(String)request.getParameter("id");
        int result=personService.deleteInfo(id);
        return new JsonResult<Integer>(result);
    }


    //获取所有人员信息
    @RequestMapping(value="/getStopPerson")
    @ResponseBody
    public  Map<String,Object> getStopPerson(PersonVO personVO) {
        personVO.setStatus("冻结");
        List<PersonVO> personVOList= personService.getPerson(personVO);
        int total=personService.countAllPerson(personVO);
        Map<String,Object> map=new HashMap<>();
        map.put("rows",personVOList);
        map.put("total",total);
        return  map;
    }


    //获取所有人员信息
    @RequestMapping(value="/getOpenPerson")
    @ResponseBody
    public  Map<String,Object> getOpenPerson(PersonVO personVO) {
        personVO.setStatus("启用");
        List<PersonVO> personVOList= personService.getPerson(personVO);
        int total=personService.countAllPerson(personVO);
        Map<String,Object> map=new HashMap<>();
        map.put("rows",personVOList);
        map.put("total",total);
        return  map;
    }

    //添加开户人员信息
    @RequestMapping(value="/submitInfo")
    @ResponseBody
    public  JsonResult<Integer> submitInfo(PersonVO personVO) {
        try {
           /* for(int i=0;i<4000;i++){
                personVO.setUsername("qcc"+i);
                personVO.setRealname("秦菜菜"+i);
                personVO.setCardid("47546119930457"+(1000+i));
                personVO.setTelephone("1557594"+(1000+i));
                int rd=Math.random()>0.5?1:0;
                personVO.setSex(rd+"");
                personService.submitInfo(personVO);
            }*/
            int result = personService.submitInfo(personVO);
            if (result==1) {
                return new JsonResult<>(result);
            }else{
                return new JsonResult<>(1,"数据插入失败");
            }
        }catch (Exception e){
            return new JsonResult<>(e);
        }
    }

    @RequestMapping(value="/exportInfo")
    public  JsonResult<Integer>  downLoginLogExcel(HttpServletResponse response){
        try{
            List<PersonVO> personVOs=personService.getPersonList();
            FileUtil.downloadFile(response, personService.getExcel(personVOs));
            return new JsonResult<>(0);
        }catch (Exception e){
            return new JsonResult<>(e);
        }
    }


    @RequestMapping("/exportExcel")
    @ResponseBody
    public String exportExcel(String description, MultipartFile file,HttpServletResponse response) throws Exception {
        //判断文件是否为空
        if(file==null){
            String errDetails="文件是空的!";
            return errDetails;
        }
        //获取文件名
        String name=file.getOriginalFilename();
        if(name==null || !WDWUtil1.validateExcel(name)){
            String errDetails="文件格式不正确！请使用.xls或.xlsx后缀文档。";
            return errDetails;
        }
        ImportExecl importExecl=new ImportExecl();
        List<List<String>> list = importExecl.read(file.getInputStream(), false);
        try {
            int i=personService.importExcel(list);
        }catch (Exception e){
            return "数据上传失败";
        }

        return "SUCCESS";
    }
}