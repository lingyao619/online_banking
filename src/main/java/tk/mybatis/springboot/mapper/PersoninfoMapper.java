
package tk.mybatis.springboot.mapper;

import org.apache.ibatis.annotations.Param;
import tk.mybatis.springboot.model.Account;
import tk.mybatis.springboot.model.Person;
import tk.mybatis.springboot.model.PersonVO;
import tk.mybatis.springboot.util.MyMapper;

import java.util.List;

public interface PersoninfoMapper extends MyMapper<Person> {
    List<PersonVO> getAllPerson(@Param("rows") int rows,@Param("page") int page);

    int countAllPerson(@Param("status") String status);

    List<PersonVO> getPerson(@Param("rows") int rows,@Param("page") int page,@Param("status") String status );

    List<PersonVO> getPersonList();
}
